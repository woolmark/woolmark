import javax.microedition.midlet.MIDlet;
import javax.microedition.lcdui.Display;

/**
 * Sheep MIDlet.
 *
 * @author takkie
 */
public class Sheep extends MIDlet {

	public SheepCanvas canvas;

	public void startApp() {
		if (canvas == null) {
			canvas = new SheepCanvas();
			canvas.start();
			Display.getDisplay(this).setCurrent(canvas);
		}
	}

	public void pauseApp() {
	}

	public void destroyApp(boolean conditional) {
	}

}
